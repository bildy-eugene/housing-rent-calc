/*!
 * VueJS Housing renting calculator plugin
 * (c) 2020-2021 Eugene Zhuroff, Bildy Oy
 * Released under the MIT License.
 */

import HousingRentMain from './components/HousingRentMain.vue'

window.onload = (() => {

	const app_container = document.querySelector('#housing-rent-calc')

	if (app_container) {
		if (typeof Vue === 'undefined' || typeof Vue === 'null') {
			const vue_lib = document.createElement('script')
			const fonts = document.createElement('link')

			vue_lib.src = 'https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js'
			fonts.href = 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap'

			document.head.append(vue_lib)
			document.head.append(fonts)

			vue_lib.onload = () => housingRentCalcInit()
		} else {
			housingRentCalcInit()
		}
	}

	function housingRentCalcInit() {
		const root_component = document.createElement('housing-rent-main')
		const is_app_collapsed = app_container.dataset.collapsed || false
		const is_app_styles = app_container.dataset.styles || true
		const app_locale = app_container.dataset.locale || 'fi'

		app_container.append(root_component)

		root_component.setAttribute(':locale', `'${app_locale}'`)
		root_component.setAttribute(':collapsed', is_app_collapsed)
		root_component.setAttribute(':stylized', is_app_styles)

		new Vue({
			el: app_container,
	
			components: {
				HousingRentMain
			}
		})
	}
	
})()