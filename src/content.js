const CONTENT = [
	{
		lang: 'fi',
		title: 'Työsuhdeasunnon asuntoetulaskuri',
		collapse: {
			on: 'Laajentaa',
			off: 'Romahtaa'
		},
		headings: {
			apartment_size: 'Asunnon tiedot',
			location: 'Sijainti',
			electricity: 'Sähkö',
			heating: 'Asunnon lämmitys',
			garage: 'Autotalli- tai hallipaikka',
			total_sum: 'Antamillasi tiedoilla asuntosi verotusarvo on.'
		},
		labels: {
			square_quantity: 'Neliömäärä:'
		},
		zones: [
			{
				id: 'zone_1',
				title: 'Asunto sijaitsee alueella Helsinki 1',
				text: 'Alueeseen Helsinki 1 kuuluvat asunnot, jotka sijaitsevat postinumeroalueella',
				locs: ['00100', '00120', '00130', '00140', '00150', '00160', '00170', '00180'],
				value: { base: 291, square: 12.8, zone: 1 }
			},
			{
				id: 'zone_2',
				title: 'Asunto sijaitsee alueella Helsinki 2',
				text: 'Alueeseen Helsinki 2 kuuluvat asunnot, jotka sijaitsevat postinumeroalueella',
				locs: ['00200', '00210', '00220', '00240', '00250', '00260', '00270', '00280', '00290', '00300', '00310', '00320', '00330', '00340', '00350', '00380', '00400', '00440', '00500', '00510', '00520', '00530', '00540', '00550', '00570', '00610', '00660', '00670', '00680', '00830', '00850'],
				value: { base: 287, square: 11.7, zone: 2 }
			},
			{
				id: 'zone_3',
				title: 'Asunto sijaitsee Espoossa, Kauniaisissa tai alueella Helsinki 3',
				text: 'Alueeseen Helsinki 3 kuuluvat asunnot, jotka sijaitsevat postinumeroalueella',
				locs: ['00360', '00370', '00390', '00410', '00420', '00430', '00560', '00600', '00620', '00630', '00640', '00650', '00700', '00720', '00780', '00790', '00800', '00810', '00840', '00870'],
				value: { base: 256, square: 10.8, zone: 3 }
			},
			{
				id: 'zone_4',
				title: 'Asunto sijaitsee Vantaalla tai alueella Helsinki 4',
				text: 'Alueeseen Helsinki 3 kuuluvat asunnot, jotka sijaitsevat postinumeroalueella',
				locs: ['00190', '00580', '00690', '00710', '00730', '00740', '00750', '00760', '00770', '00820', '00860', '00880', '00900', '00910', '00920', '00930', '00940', '00950', '00960', '00970', '00980', '00990'],
				value: { base: 205, square: 10.8, zone: 4 }
			},
			{
				id: 'zone_5',
				title: 'Asunto sijaitsee muulla Helsingissä tai',
				locs: ['Jyväskylässä', 'Kuopiossa', 'Lahdessa', 'Oulussa', 'Tampereella', 'Turussa', 'Hyvinkäällä', 'Järvenpäässä', 'Keravalla', 'Kirkkonummella', 'Nurmijärvellä', 'Riihimäellä', 'Sipoossa', 'Tuusulassa', 'Vihdissä'],
				value: { base: 191, square: 8.9, zone: 5 }
			},
			{
				id: 'zone_6',
				title: 'Asunto sijaitsee muualla Suomessa',
				value: { base: 166, square: 8, zone: 6 }
			}
		],
		electricity_payment: [
			{
				id: 'electricity_1',
				title: 'Maksan itse käyttösähkön',
				value: 0
			},
			{
				id: 'electricity_2',
				title: 'Käyttösähkö sisältyy vuokraan',
				value: 0.89
			}
		],
		heating_payment: [
			{
				id: 'heating_1',
				title: 'Lämmitys sisältyy vuokraan',
				value: 0
			},
			{
				id: 'heating_2',
				title: 'Maksan itse lämmityksen',
				value: 1.21
			}
		],
		garage_payment: [
			{
				id: 'garage_cold',
				title: 'Vuokraan sisältyy lämmin autotalli- tai autohallipaikka',
				value: { a: 85, b: 55 }
			},
			{
				id: 'garage_heat',
				title: 'Vuokraan sisältyy kylmä autotalli- tai hallipaikka',
				value: { a: 55, b: 44 }
			}
		]
	}
]

export default CONTENT