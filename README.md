# VueJS Housing renting calculator plugin

## Install Yarn (if it not installed)
```
npm i yarn -g
```

## Cloning repository

```
git clone https://bildy-eugene@bitbucket.org/bildy-eugene/housing-rent-calc.git
```

## Installing packages

```
yarn install
```

## Serve locally

```
yarn watch
```

## Build

```
yarn build
```

## Initialization

```
<div id="housing-rent-calc"></div>
<script src="[PATH_TO]/housing-rent-main.min.js"></script>
```

## Initialization options
#### Set data-attributes for #housing-rent-calc:

```
data-locale="en|fi" (Interface language. Default: 'fi')
data-styles="true|false" (Use basic CSS-styles. Default: true)
data-collapsed="true|false" (Make the app collapsible/expandable. Default: false)
```