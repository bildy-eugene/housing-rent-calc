const PATH = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
	entry: {
		app: './src/index.js'
	},

	output: {
		filename: 'housing-rent-main.min.js',
		path: PATH.resolve(__dirname, './dist'),
		publicPath: '/dist'
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: '/node_modules/'
			},

			{
        test: /\.vue$/,
        loader: 'vue-loader'
			},
			
			{
				test: /\.scss$/,
				use: [
					'vue-style-loader',
          'css-loader',
          'sass-loader'
				]
			}
		]
	},

	plugins: [
		new VueLoaderPlugin()
  ],

	devServer: {
		overlay: true
	}
}